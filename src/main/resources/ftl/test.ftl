<?xml version="1.0" encoding="utf-8"?>
<?mso-application progid="Word.Document"?>

<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>
                <Relationship Id="rId2" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>
                <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="word/document.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings" Target="settings.xml"/>
                <Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>
                <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXml" Target="../customXml/item1.xml"/>
                <Relationship Id="rId6" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" Target="theme/theme1.xml"/>
                <Relationship Id="rId5" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable" Target="fontTable.xml"/>
                <Relationship Id="rId4" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings" Target="webSettings.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" mc:Ignorable="w14 w15 w16se wp14">
                <w:body>
                    <w:p w14:paraId="1283D23F" w14:textId="1EE64CDF" w:rsidR="006B7826" w:rsidRPr="00324F3C" w:rsidRDefault="00610582" w:rsidP="00324F3C">
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>内蒙古华瑞检验检测有限公司</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="041FC37C" w14:textId="2513D943" w:rsidR="00610582" w:rsidRPr="00324F3C" w:rsidRDefault="00610582" w:rsidP="00324F3C">
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>委</w:t>
                        </w:r>
                        <w:r w:rsidR="00A7363F" w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>托</w:t>
                        </w:r>
                        <w:r w:rsidR="00A7363F" w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>检</w:t>
                        </w:r>
                        <w:r w:rsidR="00A7363F" w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>验</w:t>
                        </w:r>
                        <w:r w:rsidR="00A7363F" w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>协</w:t>
                        </w:r>
                        <w:r w:rsidR="00A7363F" w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>议</w:t>
                        </w:r>
                        <w:r w:rsidR="00A7363F" w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>书</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="1A9DA89D" w14:textId="25DE7DF1" w:rsidR="00670A45" w:rsidRPr="00A05E28" w:rsidRDefault="00324F3C" w:rsidP="00426044">
                        <w:pPr>
                            <w:spacing w:line="276" w:lineRule="auto"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>样品</w:t>
                        </w:r>
                        <w:r w:rsidR="004D1483" w:rsidRPr="000C4F5B">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>编号</w:t>
                        </w:r>
                        <w:r w:rsidR="004D1483" w:rsidRPr="00F55339">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>：</w:t>
                        </w:r>
                        <w:r w:rsidR="00A7363F">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidR="004B4D86">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t>${orderForm.id}</w:t>
                        </w:r>
                        <w:r w:rsidR="00A7363F">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t xml:space="preserve">               </w:t>
                        </w:r>
                        <w:r w:rsidR="004B4D86">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidR="00EC2897">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidR="004B4D86">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t xml:space="preserve">          </w:t>
                        </w:r>
                        <w:r w:rsidR="00610582" w:rsidRPr="00AB0E86">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>编号</w:t>
                        </w:r>
                        <w:r w:rsidR="00610582" w:rsidRPr="00F55339">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>：</w:t>
                        </w:r>
                        <w:r w:rsidR="00610582" w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>4HR/030-ZL</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="a3"/>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:jc w:val="center"/>
                            <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="564"/>
                            <w:gridCol w:w="1124"/>
                            <w:gridCol w:w="843"/>
                            <w:gridCol w:w="1255"/>
                            <w:gridCol w:w="1122"/>
                            <w:gridCol w:w="1404"/>
                            <w:gridCol w:w="1397"/>
                            <w:gridCol w:w="1358"/>
                            <w:gridCol w:w="1389"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="1AE106BC" w14:textId="77777777" w:rsidTr="00BF4BDF">
                            <w:trPr>
                                <w:trHeight w:val="662"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2BE87D4A" w14:textId="4568E533" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="006B5B08" w:rsidP="006B5B08">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:ind w:rightChars="5" w:right="10"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>委托单位信息</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1967" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="314D90C4" w14:textId="77777777" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="00E077EC">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>委托单位名称</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="601D2D96" w14:textId="4899047A" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="00E077EC">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>（或委托人姓名）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7925" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="22EA8AF1" w14:textId="10D69ECE" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="004B4D86" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${clientForm.company!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="7DE98853" w14:textId="77777777" w:rsidTr="00BF4BDF">
                            <w:trPr>
                                <w:trHeight w:val="416"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="45CC2334" w14:textId="77777777" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1967" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="209AD811" w14:textId="52522B64" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="00E077EC">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>生产单位名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7925" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6EB7028B" w14:textId="5D8E57FA" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="004B4D86" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${sampleForm.manufact!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00BF4BDF" w:rsidRPr="004D1483" w14:paraId="75F1C742" w14:textId="77777777" w:rsidTr="003178E1">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="031FE5D9" w14:textId="77777777" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1967" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="08FAEF85" w14:textId="392C5B1A" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="00E077EC">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>委托单位详细地址</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="5178" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="68D35E1A" w14:textId="5AE78561" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="004B4D86" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${clientForm.address!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1358" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0FCDF65F" w14:textId="4AB1BFFF" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="00E077EC">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>邮 编</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1389" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6B7155B4" w14:textId="66654C86" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="004B4D86" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${clientForm.email!}</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                        <w:t></w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="5411A854" w14:textId="77777777" w:rsidTr="003178E1">
                            <w:trPr>
                                <w:trHeight w:val="454"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="57762C55" w14:textId="77777777" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7145" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="27253363" w14:textId="3C2FA865" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="005121D4" w:rsidP="005121D4">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>（本协议书信息请准确填写并核对无误，检验报告签发后将不能修改）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1358" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1D6FEE56" w14:textId="77777777" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="006F0D5B">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>联系电话</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="59312AAA" w14:textId="50A44B27" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="00E077EC" w:rsidP="006F0D5B">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">/手 </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>机</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1389" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4E3EA375" w14:textId="21AF15A4" w:rsidR="00E077EC" w:rsidRPr="004D1483" w:rsidRDefault="004B4D86" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                <#--<w:r>
                    <w:rPr>
                      <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                    </w:rPr>
                    <w:t>1</w:t>
                  </w:r>-->
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                        <w:t>${clientForm.tel!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="624233BA" w14:textId="77777777" w:rsidTr="003178E1">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="44737CC7" w14:textId="4E27879A" w:rsidR="006F0D5B" w:rsidRPr="00324F3C" w:rsidRDefault="006F0D5B" w:rsidP="001D0AB5">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>样品信息</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1D86A51A" w14:textId="3E5D6C7D" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="006F0D5B" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>样品名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2098" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1C0D2958" w14:textId="1DC4D8AE" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="004B4D86" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${sampleForm.name!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1122" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3FF7D0C3" w14:textId="4FF0F3B1" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="006F0D5B" w:rsidP="006F0D5B">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">商 </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>标</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2801" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2110A0BC" w14:textId="472C395A" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="004B4D86" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                <#--<w:r>
                    <w:rPr>
                      <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                    </w:rPr>
                    <w:t>泰</w:t>
                  </w:r>
                  <w:proofErr w:type="gramStart"/>-->
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${sampleForm.trademark!}</w:t>
                                    </w:r>
                                    <w:proofErr w:type="gramEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1358" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="15F30240" w14:textId="67D1665A" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="004B4D86" w:rsidP="004B4D86">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004B4D86">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>质量等级</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1389" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="26A9FC56" w14:textId="58E6EC0C" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="00BF4BDF" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${sampleForm.level!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="38D4D870" w14:textId="77777777" w:rsidTr="003178E1">
                            <w:trPr>
                                <w:trHeight w:val="584"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="20C74129" w14:textId="77777777" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="006F0D5B" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="724B2E9A" w14:textId="77777777" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="006F0D5B" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>生产日期</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="171B8C87" w14:textId="767C7362" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="006F0D5B" w:rsidP="006F0D5B">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>或批号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2098" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="47E9A7AE" w14:textId="10F63484" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="00BF4BDF" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${sampleForm.dataBatchno!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1122" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="69D28D1C" w14:textId="5DA71981" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="00BE0AE8" w:rsidP="00BE0AE8">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>规格型号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2801" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4D7C4F47" w14:textId="72CB82B3" w:rsidR="006F0D5B" w:rsidRPr="004D1483" w:rsidRDefault="00BF4BDF" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${sampleForm.specification!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1358" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="34FA240C" w14:textId="7418379A" w:rsidR="006F0D5B" w:rsidRPr="004A44AE" w:rsidRDefault="00D94304" w:rsidP="00D94304">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004A44AE">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>产品标准号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1389" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7B459E3F" w14:textId="21119BA6" w:rsidR="006F0D5B" w:rsidRPr="003178E1" w:rsidRDefault="00BF4BDF" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="003178E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                        </w:rPr>
                                        <w:t>

                                        </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00BF4BDF" w:rsidRPr="004D1483" w14:paraId="19CF4944" w14:textId="77777777" w:rsidTr="003178E1">
                            <w:trPr>
                                <w:trHeight w:val="664"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="58801CC6" w14:textId="77777777" w:rsidR="00D94304" w:rsidRPr="004D1483" w:rsidRDefault="00D94304" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="45DCBAD6" w14:textId="7AC7AF2E" w:rsidR="00D94304" w:rsidRPr="004D1483" w:rsidRDefault="00D94304" w:rsidP="00D94304">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>检验类别</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6021" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6D3FA921" w14:textId="06A52659" w:rsidR="00D94304" w:rsidRPr="00236D68" w:rsidRDefault="00577ACC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="862873241"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                      <#if sampleForm.checkType?default('')?contains("1")>
　　                       <w:sdtContent>
                          <w:r w:rsidRPr="00236D68">
                              <w:rPr>
                                  <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                  <w:szCs w:val="21"/>
                              </w:rPr>
                              <w:sym w:font="Wingdings 2" w:char="F052"/>
                          </w:r>
                      </w:sdtContent>
                      <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                      </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="004A5701" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>委托检验（给出符合标准与否的检验报告）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="2A2D7F2E" w14:textId="0BACEA4C" w:rsidR="004A5701" w:rsidRPr="00236D68" w:rsidRDefault="00577ACC" w:rsidP="00577ACC">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1258638104"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                      <#if sampleForm.checkType?default('')?contains("2")>
　　                       <w:sdtContent>
                          <w:r w:rsidRPr="00236D68">
                              <w:rPr>
                                  <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                  <w:szCs w:val="21"/>
                              </w:rPr>
                              <w:sym w:font="Wingdings 2" w:char="F052"/>
                          </w:r>
                      </w:sdtContent>
                      <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                      </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="004A5701" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>委托测试（仅提供测试数据报告单）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1358" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2E74D410" w14:textId="13206785" w:rsidR="00D94304" w:rsidRPr="004A44AE" w:rsidRDefault="00D94304" w:rsidP="00D42C20">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004A44AE">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>样品数量</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1389" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="52F8472C" w14:textId="6501D6AD" w:rsidR="00D94304" w:rsidRPr="004D1483" w:rsidRDefault="003178E1" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${sampleForm.quantity!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00577ACC" w:rsidRPr="004D1483" w14:paraId="4B43E69F" w14:textId="0A319A5D" w:rsidTr="00BF4BDF">
                            <w:trPr>
                                <w:trHeight w:val="985"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="403A66DB" w14:textId="77777777" w:rsidR="00577ACC" w:rsidRPr="004D1483" w:rsidRDefault="00577ACC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1B88BF65" w14:textId="5F6F90FB" w:rsidR="00577ACC" w:rsidRPr="004D1483" w:rsidRDefault="00577ACC" w:rsidP="00D94304">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>样品状态</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8768" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="75D1DDF5" w14:textId="3A318E5B" w:rsidR="00577ACC" w:rsidRPr="00236D68" w:rsidRDefault="00577ACC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="864428"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                      <#if sampleForm.sampleContainer?default('')?contains("1")>
　　                       <w:sdtContent>
                          <w:r w:rsidRPr="00236D68">
                              <w:rPr>
                                  <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                  <w:szCs w:val="21"/>
                              </w:rPr>
                              <w:sym w:font="Wingdings 2" w:char="F052"/>
                          </w:r>
                      </w:sdtContent>
                      <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                      </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>玻璃瓶罐</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1917127602"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleContainer?default('')?contains("2")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">塑料瓶罐  </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1227955955"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleContainer?default('')?contains("3")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">塑料袋  </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="2021661002"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                   <#if sampleForm.sampleContainer?default('')?contains("4")>
　　                       <w:sdtContent>
                       <w:r w:rsidRPr="00236D68">
                           <w:rPr>
                               <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                               <w:szCs w:val="21"/>
                           </w:rPr>
                           <w:sym w:font="Wingdings 2" w:char="F052"/>
                       </w:r>
                   </w:sdtContent>
                   <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                   </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>金属瓶罐</w:t>
                                    </w:r>
                                    <w:r w:rsidR="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1956290376"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleContainer?default('')?contains("5")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>纸杯盒</w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1719091568"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleContainer?default('')?contains("6")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>纸袋</w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1969463381"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleContainer?default('')?contains("9")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>其他：${sampleForm.containerText!}</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="4A17E4C7" w14:textId="21FC9975" w:rsidR="00577ACC" w:rsidRPr="00236D68" w:rsidRDefault="00577ACC" w:rsidP="00577ACC">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1922449741"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleState?default('')?contains("1")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>固态</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-693999453"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleState?default('')?contains("2")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">半固态 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-185606574"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleState?default('')?contains("3")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">液态 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1962224865"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleState?default('')?contains("4")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">气态 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-2010743153"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleState?default('')?contains("5")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>粉末态</w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-1646733844"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.sampleState?default('')?contains("9")>
　　                       <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>其他：${sampleForm.stateText!}</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="2BFB8ED2" w14:textId="78328C91" w:rsidR="00577ACC" w:rsidRPr="004A5701" w:rsidRDefault="00577ACC" w:rsidP="00236D68">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="138317020"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.storageStatus?default('')?contains("1")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>常温</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1622812050"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.storageStatus?default('')?contains("2")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>避光</w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-487481690"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.storageStatus?default('')?contains("3")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">干燥 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-2079041538"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.storageStatus?default('')?contains("4")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">冷藏 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-172804754"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.storageStatus?default('')?contains("5")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">冷冻 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="007F40C7" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-1808238893"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.storageStatus?default('')?contains("9")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>其他：${sampleForm.storageText!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="209F84B6" w14:textId="77777777" w:rsidTr="00BF4BDF">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="06F33D68" w14:textId="77777777" w:rsidR="00D94304" w:rsidRPr="004D1483" w:rsidRDefault="00D94304" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3C876884" w14:textId="04AA1892" w:rsidR="00D94304" w:rsidRPr="004D1483" w:rsidRDefault="00D94304" w:rsidP="00D94304">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>标签状态</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8768" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="15A61C7C" w14:textId="3B8AF8E0" w:rsidR="00D94304" w:rsidRPr="004D1483" w:rsidRDefault="00236D68" w:rsidP="00236D68">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-1137189130"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.identifier?default('')?contains("1")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">信息完整 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">         </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="2005848465"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.identifier?default('')?contains("2")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">信息不全 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">         </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-355117293"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.identifier?default('')?contains("3")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">无标签 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">       </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="898792012"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.identifier?default('')?contains("9")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>其他：${sampleForm.identifierText!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="3526BAB0" w14:textId="77777777" w:rsidTr="00BF4BDF">
                            <w:trPr>
                                <w:trHeight w:val="1418"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="22B2EE6B" w14:textId="2828EA92" w:rsidR="00E3187F" w:rsidRPr="004D1483" w:rsidRDefault="00E3187F" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>检验要求</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4344" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="78A3C75F" w14:textId="15943A51" w:rsidR="00E3187F" w:rsidRDefault="006D6D3D" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>检验依据：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="00236D68" w:rsidRPr="00236D68">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>
                        <#list judgeList as e>
                            ${e.name}
                        </#list>
                                        </w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="443BE012" w14:textId="77777777" w:rsidR="00236D68" w:rsidRDefault="00236D68" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="3E908985" w14:textId="329C9A75" w:rsidR="00236D68" w:rsidRDefault="00236D68" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1534077929"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.standardType?default('')?contains("1")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>由本机构选定合适标准</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="66E8484D" w14:textId="5BC45D14" w:rsidR="00236D68" w:rsidRDefault="00236D68" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1225881410"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.standardType?default('')?contains("9")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>指定检测依据标准或方法：${sampleForm.standardText!}</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="79E9B0D9" w14:textId="52567E81" w:rsidR="00236D68" w:rsidRPr="00236D68" w:rsidRDefault="00236D68" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-1138413929"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if sampleForm.standardType?default('')?contains("2")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>同意用本所确定的非标准方法</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="5548" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="353A4977" w14:textId="77777777" w:rsidR="00E3187F" w:rsidRDefault="00363BB6" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>检测项目：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="524A4563" w14:textId="77777777" w:rsidR="00236D68" w:rsidRDefault="00236D68" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="479EE0E6" w14:textId="2DF9AAD8" w:rsidR="00236D68" w:rsidRPr="00D778CD" w:rsidRDefault="00236D68" w:rsidP="00236D68">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:ind w:firstLineChars="1000" w:firstLine="2108"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>详见附表</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00BF4BDF" w:rsidRPr="004D1483" w14:paraId="21E4F5F2" w14:textId="77777777" w:rsidTr="00BF4BDF">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="29D4B154" w14:textId="59AF4398" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="00F634E0" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>报告交付</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5FA3B09D" w14:textId="589ABDE6" w:rsidR="00F634E0" w:rsidRPr="00D778CD" w:rsidRDefault="00F634E0" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>交付方式</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3220" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="45A2C3E8" w14:textId="47E6EC0F" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="00236D68" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-451243041"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.deliveryType?default('')?contains("1")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">自取 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="864953810"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.deliveryType?default('')?contains("2")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">特快专递  </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-2094843301"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.deliveryType?default('')?contains("3")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>传真</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1404" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="04EC82FE" w14:textId="61C0C8FE" w:rsidR="00F634E0" w:rsidRPr="00D778CD" w:rsidRDefault="00F634E0" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>报告格式</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4144" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3529DBE0" w14:textId="70A08B8A" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="00236D68" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="32858711"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.format?default('')?contains("1")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">正式报告 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-851794628"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.format?default('')?contains("2")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">报告单 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1751000408"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.format?default('')?contains("3")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>其他：</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00BF4BDF" w:rsidRPr="004D1483" w14:paraId="6CBD611D" w14:textId="77777777" w:rsidTr="003178E1">
                            <w:trPr>
                                <w:trHeight w:val="408"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="67BC0906" w14:textId="77777777" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="00F634E0" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="33B3094A" w14:textId="15D4CDDF" w:rsidR="00F634E0" w:rsidRPr="00D778CD" w:rsidRDefault="00F634E0" w:rsidP="00E83F5D">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>报告份数</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6021" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="71761EA0" w14:textId="5AD9855C" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="006E5DAC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-968809562"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.num?default('')?contains("1")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">1份 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="559911733"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.num?default('')?contains("2")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">2份 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1329556257"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.num?default('')?contains("3")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>3份</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1358" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="74799016" w14:textId="77777777" w:rsidR="00F634E0" w:rsidRPr="00D778CD" w:rsidRDefault="00F634E0" w:rsidP="00F634E0">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>样品</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="5B81DFF4" w14:textId="0296F675" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="00F634E0" w:rsidP="00F634E0">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>处理</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1389" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="207968FC" w14:textId="453D0DE3" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="006E5DAC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-1441684929"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.sampleDeal?default('')?contains("1")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>退还</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00BF4BDF" w:rsidRPr="004D1483" w14:paraId="7A6BC3D6" w14:textId="77777777" w:rsidTr="003178E1">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="38438002" w14:textId="77777777" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="00F634E0" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1092AAEA" w14:textId="37CCDBD2" w:rsidR="00F634E0" w:rsidRPr="00D778CD" w:rsidRDefault="00F634E0" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>交付日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6021" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5C236D7A" w14:textId="39BE4FD1" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="006E5DAC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1758332207"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.deliveryDate?default('')?contains("1")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">5个工作日 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="1938559364"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="0"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.deliveryDate?default('')?contains("2")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">7个工作日 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-1590611632"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.deliveryDate?default('')?contains("9")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>加急 ${deliveryForm.dateText!}日</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1358" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="562BCDD1" w14:textId="77777777" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="00F634E0" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1389" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6983A55D" w14:textId="316FF484" w:rsidR="00F634E0" w:rsidRPr="004D1483" w:rsidRDefault="006E5DAC" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:sdt>
                                        <w:sdtPr>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                                <w:szCs w:val="21"/>
                                            </w:rPr>
                                            <w:id w:val="-1533794363"/>
                                            <w14:checkbox>
                                                <w14:checked w14:val="1"/>
                                                <w14:checkedState w14:val="0052" w14:font="Wingdings 2"/>
                                                <w14:uncheckedState w14:val="2610" w14:font="MS Gothic"/>
                                            </w14:checkbox>
                                        </w:sdtPr>
                    <#if deliveryForm.sampleDeal?default('')?contains("2")>
　　                     <w:sdtContent>
                        <w:r w:rsidRPr="00236D68">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sym w:font="Wingdings 2" w:char="F052"/>
                        </w:r>
                    </w:sdtContent>
                    <#else>
                          <w:sdtContent>
                              <w:r w:rsidRPr="00236D68">
                                  <w:rPr>
                                      <w:rFonts w:ascii="MS Gothic" w:eastAsia="MS Gothic" w:hAnsi="MS Gothic" w:hint="eastAsia"/>
                                      <w:szCs w:val="21"/>
                                  </w:rPr>
                                  <w:t>☐</w:t>
                              </w:r>
                          </w:sdtContent>
                    </#if>
                                    </w:sdt>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>直接处理</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="0E126047" w14:textId="77777777" w:rsidTr="00BF4BDF">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1344EDE5" w14:textId="3E65E1B1" w:rsidR="000E79E5" w:rsidRPr="004D1483" w:rsidRDefault="000E79E5" w:rsidP="001D0AB5">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>其他</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="56671520" w14:textId="43443EB7" w:rsidR="000E79E5" w:rsidRPr="00D778CD" w:rsidRDefault="000E79E5" w:rsidP="000E79E5">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>测试费用</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3220" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1553B0D4" w14:textId="17029EA0" w:rsidR="000E79E5" w:rsidRPr="004D1483" w:rsidRDefault="003178E1" w:rsidP="000E79E5">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${orderForm.discountCharge!}</w:t>
                                    </w:r>
                                    <w:r w:rsidR="000E79E5" w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>元</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="5548" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="42994C56" w14:textId="55B413F6" w:rsidR="000E79E5" w:rsidRPr="004D1483" w:rsidRDefault="000E79E5" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>其他约定或说明：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="003178E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${deliveryForm.deliveryDes!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="004B4D86" w:rsidRPr="004D1483" w14:paraId="3020B9BA" w14:textId="77777777" w:rsidTr="00BF4BDF">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="564" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="72043FE2" w14:textId="77777777" w:rsidR="000E79E5" w:rsidRPr="004D1483" w:rsidRDefault="000E79E5" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1124" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2813B4DE" w14:textId="34A6450C" w:rsidR="000E79E5" w:rsidRPr="00D778CD" w:rsidRDefault="000E79E5" w:rsidP="000E79E5">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">备 </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00D778CD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                        </w:rPr>
                                        <w:t>注</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8768" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3147F221" w14:textId="561A918A" w:rsidR="000E79E5" w:rsidRPr="004D1483" w:rsidRDefault="003178E1" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>${deliveryForm.deliveryRemark!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="000E79E5" w:rsidRPr="004D1483" w14:paraId="778FF2DC" w14:textId="77777777" w:rsidTr="0039611E">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10456" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="24A32A13" w14:textId="77777777" w:rsidR="000E79E5" w:rsidRPr="004D1483" w:rsidRDefault="000E79E5" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>委托方保证所提供的所有相关信息、资料和实物的真实性，并承担相应责任。</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="70A4A662" w14:textId="68C717A6" w:rsidR="000E79E5" w:rsidRPr="004D1483" w:rsidRDefault="000E79E5" w:rsidP="00670A45">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004D1483">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>委托方同意检测并按此协议的条件进行，同时支付所需的费用和提供必要的合作。</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="000E79E5" w:rsidRPr="004D1483" w14:paraId="1170ADBA" w14:textId="77777777" w:rsidTr="003344A3">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10456" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="220ABFE6" w14:textId="6E64DD6F" w:rsidR="000E79E5" w:rsidRPr="00324F3C" w:rsidRDefault="000E79E5" w:rsidP="00324F3C">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>委托方确认：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                       </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">日期： </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">年 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">月 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>日</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="000E79E5" w:rsidRPr="004D1483" w14:paraId="4F96937D" w14:textId="77777777" w:rsidTr="003344A3">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10456" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="64A2AB68" w14:textId="2E8FD68C" w:rsidR="000E79E5" w:rsidRPr="00D778CD" w:rsidRDefault="000E79E5" w:rsidP="00324F3C">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>内蒙古华瑞业务人员：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">               </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">日期： </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">年 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">月 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>日</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="000E79E5" w:rsidRPr="004D1483" w14:paraId="519A8E1F" w14:textId="77777777" w:rsidTr="003344A3">
                            <w:trPr>
                                <w:trHeight w:val="397"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10456" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="108ECEA7" w14:textId="7611FE5A" w:rsidR="000E79E5" w:rsidRPr="00D778CD" w:rsidRDefault="000E79E5" w:rsidP="00324F3C">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="gramStart"/>
                                    <w:r w:rsidRPr="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>接样人员</w:t>
                                    </w:r>
                                    <w:proofErr w:type="gramEnd"/>
                                    <w:r w:rsidRPr="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>确认：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                     </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">日期： </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">年 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">月 </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00324F3C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>日</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="7A79E7BF" w14:textId="77777777" w:rsidR="00D42C20" w:rsidRPr="000E5689" w:rsidRDefault="00763A75" w:rsidP="00324F3C">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:left="850" w:hangingChars="472" w:hanging="850"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>说明：</w:t>
                        </w:r>
                        <w:r w:rsidR="00001B04" w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>1、检测报告需要更改·须在报告交付前以书面方式提出申请，填写更改原因，更改内容，报告签发后不得随意更改。在检测过程中若需补送有关</w:t>
                        </w:r>
                        <w:r w:rsidR="00D42C20" w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>样品、资料，或实验样品出现整改。视情况适当延长检测周期。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="73936501" w14:textId="28DBE021" w:rsidR="00D42C20" w:rsidRPr="000E5689" w:rsidRDefault="00D42C20" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="540"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>2、发出检测报告后20天内仍未取回的样品，本公司不负保管责任；保质期短的样品请务必及时领会，超过保质期不予保留。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="163220BE" w14:textId="690BCC1B" w:rsidR="00D42C20" w:rsidRPr="000E5689" w:rsidRDefault="00D42C20" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="540"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>3、除非另有约定，费用未付清，本公司有权拒发检验报告；遇灾害或其他不可抗力，本公司有权拖迟执行或取消本协议。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="04A21FD9" w14:textId="6F092ED4" w:rsidR="00D42C20" w:rsidRPr="000E5689" w:rsidRDefault="00D42C20" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="540"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>4、《委托检验协议书》作为领取检验报告及样品的凭证，领取检验报告</w:t>
                        </w:r>
                        <w:r w:rsidR="00324F3C" w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>前</w:t>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>须结清检测费用。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="64673CCF" w14:textId="1922EDCC" w:rsidR="00D42C20" w:rsidRPr="000E5689" w:rsidRDefault="00D42C20" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="540"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>5、若对报告有异议，应于收到报告之日起</w:t>
                        </w:r>
                        <w:r w:rsidR="007D1693" w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>15天内提出复检申请，逾期不予受理（样品保存期为报告发出后15日）。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="6C0962C7" w14:textId="17EEF7BB" w:rsidR="007D1693" w:rsidRPr="000E5689" w:rsidRDefault="007D1693" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="540"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>6、支票填写单位：内蒙古华瑞检验检测有限公司</w:t>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                        <w:bookmarkEnd w:id="0"/>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>纳税人识别号：9</w:t>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>1150100MA0NKECW3J</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="45A28BF0" w14:textId="50D029A2" w:rsidR="007D1693" w:rsidRPr="000E5689" w:rsidRDefault="007D1693" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="540"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t xml:space="preserve">   </w:t>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>开户行：招商银行股份有限公司呼和浩特大学东街支行</w:t>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>账号：471901136710101</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="5CDA01F1" w14:textId="74F16506" w:rsidR="007D1693" w:rsidRPr="000E5689" w:rsidRDefault="007D1693" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="540"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>7、联系电话：0471-4675658</w:t>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t xml:space="preserve">传 </w:t>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>真：0471-4675658</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="6BBD7F54" w14:textId="13262BD3" w:rsidR="007D1693" w:rsidRPr="000E5689" w:rsidRDefault="007D1693" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="540"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="15"/>
                                <w:szCs w:val="15"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000E5689">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>8、单位地址：内蒙古赛罕区东方领海小区商业楼4楼</w:t>
                        </w:r>
                    </w:p>

                    <w:p w14:paraId="78259DB4" w14:textId="77777777" w:rsidR="00A24942" w:rsidRPr="00324F3C" w:rsidRDefault="00A24942" w:rsidP="00A24942">
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00A24942">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>附表：</w:t>
                        </w:r>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>内蒙古华瑞检验检测有限公司</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="5620067D" w14:textId="6195D216" w:rsidR="00A24942" w:rsidRPr="00324F3C" w:rsidRDefault="00A24942" w:rsidP="00A24942">
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>委托检验协议书</w:t>
                        </w:r>
                        <w:r w:rsidR="005F7277">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>检验</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>项目表</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="25A5CC3E" w14:textId="624C1CDA" w:rsidR="00A24942" w:rsidRDefault="00A24942" w:rsidP="005F7277">
                        <w:pPr>
                            <w:spacing w:line="276" w:lineRule="auto"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00AB0E86">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>编号</w:t>
                        </w:r>
                        <w:r w:rsidRPr="00F55339">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>：</w:t>
                        </w:r>
                        <w:r w:rsidRPr="00324F3C">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>4HR/030-ZL</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="7F6F074A" w14:textId="45BD15E0" w:rsidR="005F7277" w:rsidRPr="00A05E28" w:rsidRDefault="005F7277" w:rsidP="00A24942">
                        <w:pPr>
                            <w:spacing w:line="276" w:lineRule="auto"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>样品</w:t>
                        </w:r>
                        <w:r w:rsidRPr="000C4F5B">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>编号</w:t>
                        </w:r>
                        <w:r w:rsidRPr="00F55339">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> 45</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="a3"/>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="846"/>
                            <w:gridCol w:w="3544"/>
                            <w:gridCol w:w="3452"/>
                            <w:gridCol w:w="2614"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00A24942" w14:paraId="006428D6" w14:textId="77777777" w:rsidTr="005F7277">
                            <w:trPr>
                                <w:trHeight w:val="466"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="846" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3DBD510A" w14:textId="196BD805" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="005F7277">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>序号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3544" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="22DAC846" w14:textId="04C4B236" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="005F7277">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>检验项目</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3452" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3E41DE87" w14:textId="369DF852" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="005F7277">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>检验方法</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2614" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0002A442" w14:textId="57981B2B" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="005F7277">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>标准要求</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00A24942" w14:paraId="78CE2EE0" w14:textId="77777777" w:rsidTr="005F7277">
                            <w:trPr>
                                <w:trHeight w:val="416"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="846" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="1D7F3758" w14:textId="01571E5C" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>1</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3544" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="347AE29D" w14:textId="4881B5EF" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>沙门氏菌</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3452" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="69548044" w14:textId="7E8E0EF8" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>GB4789.4-2016</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2614" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="69C9ABBC" w14:textId="3E7AD96E" w:rsidR="005F7277" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>n=5 c=0 m=c/25g M-</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00A24942" w14:paraId="7C32043E" w14:textId="77777777" w:rsidTr="005F7277">
                            <w:trPr>
                                <w:trHeight w:val="422"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="846" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="18E93094" w14:textId="06346BD1" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>2</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3544" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="44F95224" w14:textId="050842C7" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>金黄色葡萄球菌，CFU</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/g</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3452" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="2BD107A5" w14:textId="687614F7" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>G</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>B4789.10-2016(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>第二法</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2614" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="0D1F133F" w14:textId="60D87117" w:rsidR="00A24942" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>n=5 c=1 m=100 M=1000</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="005F7277" w14:paraId="47A5464D" w14:textId="77777777" w:rsidTr="005F7277">
                            <w:trPr>
                                <w:trHeight w:val="422"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="846" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="2CACCD04" w14:textId="2A86D7E2" w:rsidR="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>3</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3544" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="7C41CACC" w14:textId="488B4437" w:rsidR="005F7277" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>单核细胞增生李斯</w:t>
                                    </w:r>
                                    <w:proofErr w:type="gramStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>特</w:t>
                                    </w:r>
                                    <w:proofErr w:type="gramEnd"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>氏菌</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3452" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="030DE4BB" w14:textId="48220695" w:rsidR="005F7277" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>GB4789.30-2016（第一法）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2614" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="470C8DB5" w14:textId="52C89577" w:rsidR="005F7277" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>n</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>=5 c=0 m=0/25g M-</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="005F7277" w14:paraId="13C03AD7" w14:textId="77777777" w:rsidTr="00317BF5">
                            <w:trPr>
                                <w:trHeight w:val="422"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10456" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="34BEB8B1" w14:textId="5D583274" w:rsidR="005F7277" w:rsidRPr="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="005F7277" w14:paraId="027DA8DA" w14:textId="77777777" w:rsidTr="00317BF5">
                            <w:trPr>
                                <w:trHeight w:val="422"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10456" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="38DFF318" w14:textId="60FCCD2D" w:rsidR="005F7277" w:rsidRDefault="005F7277" w:rsidP="005F7277">
                                    <w:pPr>
                                        <w:spacing w:line="200" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>以下空白</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="3E3B708E" w14:textId="70F4BDE3" w:rsidR="00A24942" w:rsidRPr="00A24942" w:rsidRDefault="00A24942" w:rsidP="00D42C20">
                        <w:pPr>
                            <w:spacing w:line="200" w:lineRule="atLeast"/>
                            <w:ind w:firstLineChars="300" w:firstLine="450"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="15"/>
                                <w:szCs w:val="15"/>
                            </w:rPr>
                        </w:pPr>
                        <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                        <w:bookmarkEnd w:id="0"/>
                    </w:p>

                    <w:sectPr w:rsidR="007D1693" w:rsidRPr="000E5689" w:rsidSect="00610582">
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="720" w:right="720" w:bottom="720" w:left="720" w:header="851" w:footer="992" w:gutter="0"/>
                        <w:cols w:space="425"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>

    <pkg:part pkg:name="/word/theme/theme1.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题​​">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="FFFFFF"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="44546A"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="E7E6E6"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4472C4"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="ED7D31"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="A5A5A5"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="FFC000"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="5B9BD5"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="70AD47"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0563C1"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="954F72"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="等线 Light" panose="020F0302020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游ゴシック Light"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线 Light"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="等线" panose="020F0502020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="110000"/>
                                            <a:satMod val="105000"/>
                                            <a:tint val="67000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="103000"/>
                                            <a:tint val="73000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="109000"/>
                                            <a:tint val="81000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="103000"/>
                                            <a:lumMod val="102000"/>
                                            <a:tint val="94000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="110000"/>
                                            <a:lumMod val="100000"/>
                                            <a:shade val="100000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="99000"/>
                                            <a:satMod val="120000"/>
                                            <a:shade val="78000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="63000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:solidFill>
                                <a:schemeClr val="phClr">
                                    <a:tint val="95000"/>
                                    <a:satMod val="170000"/>
                                </a:schemeClr>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="93000"/>
                                            <a:satMod val="150000"/>
                                            <a:shade val="98000"/>
                                            <a:lumMod val="102000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="98000"/>
                                            <a:satMod val="130000"/>
                                            <a:shade val="90000"/>
                                            <a:lumMod val="103000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="63000"/>
                                            <a:satMod val="120000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
                <a:extLst>
                    <a:ext uri="{05A4C25C-085E-4340-85A3-A5531E510DB2}">
                        <thm15:themeFamily xmlns:thm15="http://schemas.microsoft.com/office/thememl/2012/main" name="Office Theme" id="{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}" vid="{4A3C46E8-61CC-4603-A589-7422A47A8E4A}"></thm15:themeFamily>
                    </a:ext>
                </a:extLst>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main" mc:Ignorable="w14 w15 w16se">
                <w:zoom w:percent="140"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:activeWritingStyle w:appName="MSWord" w:lang="en-US" w:vendorID="64" w:dllVersion="131078" w:nlCheck="1" w:checkStyle="0"/>
                <w:activeWritingStyle w:appName="MSWord" w:lang="zh-CN" w:vendorID="64" w:dllVersion="131077" w:nlCheck="1" w:checkStyle="1"/>
                <w:proofState w:spelling="clean" w:grammar="clean"/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridHorizontalSpacing w:val="105"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word" w:val="15"/>
                    <w:compatSetting w:name="overrideTableStyleFontSizeAndJustification" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="enableOpenTypeFeatures" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="doNotFlipMirrorIndents" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="differentiateMultirowTableHeaders" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="00244443"/>
                    <w:rsid w:val="00001B04"/>
                    <w:rsid w:val="000C45A3"/>
                    <w:rsid w:val="000C4F5B"/>
                    <w:rsid w:val="000E5689"/>
                    <w:rsid w:val="000E79E5"/>
                    <w:rsid w:val="001B50BA"/>
                    <w:rsid w:val="001D0AB5"/>
                    <w:rsid w:val="001D337F"/>
                    <w:rsid w:val="002348E8"/>
                    <w:rsid w:val="00236D68"/>
                    <w:rsid w:val="00244443"/>
                    <w:rsid w:val="003178E1"/>
                    <w:rsid w:val="00324F3C"/>
                    <w:rsid w:val="003344A3"/>
                    <w:rsid w:val="00363BB6"/>
                    <w:rsid w:val="0039611E"/>
                    <w:rsid w:val="00426044"/>
                    <w:rsid w:val="00427EE2"/>
                    <w:rsid w:val="0048495E"/>
                    <w:rsid w:val="004A44AE"/>
                    <w:rsid w:val="004A5701"/>
                    <w:rsid w:val="004B3302"/>
                    <w:rsid w:val="004B4D86"/>
                    <w:rsid w:val="004D1483"/>
                    <w:rsid w:val="005121D4"/>
                    <w:rsid w:val="00577ACC"/>
                    <w:rsid w:val="00610582"/>
                    <w:rsid w:val="00636868"/>
                    <w:rsid w:val="00670A45"/>
                    <w:rsid w:val="006B5B08"/>
                    <w:rsid w:val="006B7826"/>
                    <w:rsid w:val="006D6D3D"/>
                    <w:rsid w:val="006E5DAC"/>
                    <w:rsid w:val="006F0D5B"/>
                    <w:rsid w:val="00757804"/>
                    <w:rsid w:val="00763A75"/>
                    <w:rsid w:val="007D1693"/>
                    <w:rsid w:val="007F40C7"/>
                    <w:rsid w:val="008629C7"/>
                    <w:rsid w:val="0087091E"/>
                    <w:rsid w:val="00A05E28"/>
                    <w:rsid w:val="00A218D4"/>
                    <w:rsid w:val="00A5362D"/>
                    <w:rsid w:val="00A7363F"/>
                    <w:rsid w:val="00AB0E86"/>
                    <w:rsid w:val="00AE501C"/>
                    <w:rsid w:val="00BE0AE8"/>
                    <w:rsid w:val="00BF4BDF"/>
                    <w:rsid w:val="00C21BF9"/>
                    <w:rsid w:val="00C63240"/>
                    <w:rsid w:val="00CF060F"/>
                    <w:rsid w:val="00D42C20"/>
                    <w:rsid w:val="00D512FB"/>
                    <w:rsid w:val="00D778CD"/>
                    <w:rsid w:val="00D94304"/>
                    <w:rsid w:val="00DC6059"/>
                    <w:rsid w:val="00E077EC"/>
                    <w:rsid w:val="00E3187F"/>
                    <w:rsid w:val="00E83F5D"/>
                    <w:rsid w:val="00EC2897"/>
                    <w:rsid w:val="00F348AF"/>
                    <w:rsid w:val="00F55339"/>
                    <w:rsid w:val="00F634E0"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1" w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5" w:accent6="accent6" w:hyperlink="hyperlink" w:followedHyperlink="followedHyperlink"/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="1026"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="1"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w14:docId w14:val="3260D73E"/>
                <w15:chartTrackingRefBased/>
                <w15:docId w15:val="{1D5CBA8F-F9A5-409E-9B00-DF1EEBEF8151}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/_rels/item1.xml.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXmlProps" Target="itemProps1.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/itemProps1.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.customXmlProperties+xml" pkg:padding="32">
        <pkg:xmlData pkg:originalXmlStandalone="no">
            <ds:datastoreItem xmlns:ds="http://schemas.openxmlformats.org/officeDocument/2006/customXml" ds:itemID="{36B35EBB-C286-471F-B462-EB6BA909E44D}">
                <ds:schemaRefs>
                    <ds:schemaRef ds:uri="http://schemas.openxmlformats.org/officeDocument/2006/bibliography"/>
                </ds:schemaRefs>
            </ds:datastoreItem>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" mc:Ignorable="w14 w15 w16se">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorEastAsia" w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi"/>
                            <w:kern w:val="2"/>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="22"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0" w:defQFormat="0" w:count="371">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid" w:uiPriority="39"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:rsid w:val="00324F3C"/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:styleId="a3">
                    <w:name w:val="Table Grid"/>
                    <w:basedOn w:val="a1"/>
                    <w:uiPriority w:val="39"/>
                    <w:rsid w:val="00670A45"/>
                    <w:tblPr>
                        <w:tblBorders>
                            <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        </w:tblBorders>
                    </w:tblPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a4">
                    <w:name w:val="Balloon Text"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a5"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="006B5B08"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a5">
                    <w:name w:val="批注框文本 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="a4"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:rsid w:val="006B5B08"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title/>
                <dc:subject/>
                <dc:creator>haor</dc:creator>
                <cp:keywords/>
                <dc:description/>
                <cp:lastModifiedBy>hr haoran</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <cp:lastPrinted>2018-07-24T12:09:00Z</cp:lastPrinted>
                <dcterms:created xsi:type="dcterms:W3CDTF">2018-07-24T12:11:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2018-07-24T12:11:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/item1.xml" pkg:contentType="application/xml" pkg:padding="32">
        <pkg:xmlData pkg:originalXmlStandalone="no">
            <b:Sources xmlns:b="http://schemas.openxmlformats.org/officeDocument/2006/bibliography" xmlns="http://schemas.openxmlformats.org/officeDocument/2006/bibliography" SelectedStyle="\APASixthEditionOfficeOnline.xsl" StyleName="APA" Version="6"></b:Sources>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" mc:Ignorable="w14 w15 w16se">
                <w:font w:name="等线">
                    <w:altName w:val="DengXian"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:altName w:val="SimSun"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Wingdings 2">
                    <w:panose1 w:val="05020102010507070707"/>
                    <w:charset w:val="02"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000000" w:usb1="10000000" w:usb2="00000000" w:usb3="00000000" w:csb0="80000000" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="MS Gothic">
                    <w:altName w:val="ＭＳ ゴシック"/>
                    <w:panose1 w:val="020B0609070205080204"/>
                    <w:charset w:val="80"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="E00002FF" w:usb1="6AC7FDFB" w:usb2="08000012" w:usb3="00000000" w:csb0="0002009F" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线 Light">
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F" w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" mc:Ignorable="w14 w15 w16se">
                <w:divs>
                    <w:div w:id="1426724665">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                    </w:div>
                </w:divs>
                <w:optimizeForBrowser/>
                <w:allowPNG/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>0</TotalTime>
                <Pages>1</Pages>
                <Words>237</Words>
                <Characters>1356</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <Lines>11</Lines>
                <Paragraphs>3</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company/>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>1590</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>16.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>
