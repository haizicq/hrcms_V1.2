package cn.haoran.datasource.annotation;

import java.lang.annotation.*;

/**
 * 多数据源注解(仅限于方法级别的注释)
 *
 * Create by hanran on 2019/6/15
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {
    String name() default "";
}
