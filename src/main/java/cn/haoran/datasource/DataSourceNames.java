package cn.haoran.datasource;

/**
 * 多数据源名称（增加多数据源，在此配置）
 *
 * Create by hanran on 2019/6/15
 */
public interface DataSourceNames {
    String FIRST = "first";
    String SECOND = "second";
}
