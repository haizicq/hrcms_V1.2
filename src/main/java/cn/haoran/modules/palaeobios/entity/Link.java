package cn.haoran.modules.palaeobios.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="外部链接对象", description="")
@TableName("biz_link")
public class Link {
    private static final long serialVersionUID = 1L;
    @TableId
    private Long id;
    @ApiModelProperty(value = "栏目id")
    private String title;
    @ApiModelProperty(value = "图片链接")
    private String imgUrl;
    @ApiModelProperty(value = "链接")
    private String linkUrl;
    private String createName;
    private LocalDateTime createDate;
    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer delete = IDeteleState.NOT_DELETE;
}
