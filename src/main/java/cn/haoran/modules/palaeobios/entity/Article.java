package cn.haoran.modules.palaeobios.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 文章的实体类
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BizArticle对象", description="")
@TableName("biz_article")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId
    private Long id;
    @ApiModelProperty(value = "栏目id")
    private Long columnId;

    @ApiModelProperty(value = "标题")
    @Length(max = 60)
    @NotEmpty
    private String title;

    @ApiModelProperty(value = "内容")
    @NotEmpty
    private String content;

    @ApiModelProperty(value = "时间")
    private LocalDateTime createDate;

    private String createName;

    private Long createId;

    @ApiModelProperty(value = "标题图片id")
    private Long titleImgId;

    @ApiModelProperty(value = "1:草稿 2:发布")
    private Integer type;

    private String titleImgUrl;
    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer delete = IDeteleState.NOT_DELETE;
}
