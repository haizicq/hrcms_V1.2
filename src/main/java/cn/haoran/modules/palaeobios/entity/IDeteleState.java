package cn.haoran.modules.palaeobios.entity;

public interface IDeteleState {
    Integer DELETE = 1;
    Integer NOT_DELETE = 0;
}
