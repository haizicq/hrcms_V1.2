package cn.haoran.modules.palaeobios.utils;


import cn.haoran.modules.palaeobios.service.ArticleService;
import cn.hutool.bloomfilter.BitMapBloomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 布隆过滤器
 */
@Component
public class BloomFilterUtils {
    @Autowired
    ArticleService articleService;
    BitMapBloomFilter filter = null;

    /**
     * 导入源id
     */
    public void importDate(){
        List<Long> data = articleService.getAllId();
        filter = new BitMapBloomFilter(10);
        for (Long i : data){
            filter.add(i.toString());
        }
    }

    public void addData(Long id){
        filter.add(id.toString());
    }
    /**
     * 是否命中
     * @param id
     * @return
     */
    public boolean hit(Long id){
        return  filter.contains(id.toString());
    }
}
