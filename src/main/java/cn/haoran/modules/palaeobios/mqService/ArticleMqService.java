package cn.haoran.modules.palaeobios.mqService;

import cn.haoran.modules.palaeobios.entity.Article;
import cn.haoran.modules.palaeobios.service.ArticleService;
import cn.haoran.modules.palaeobios.state.RabbitMqState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 异步任务写入文章操作
 */
@Service
public class ArticleMqService {
    private Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Autowired
    private  ArticleService articleService;

    @RabbitListener(queues = RabbitMqState.insert)
    private void insert(Article article){
        articleService.addArticle(article);
    }
    @RabbitListener(queues = RabbitMqState.update)
    private void update(Article article){
        articleService.updateArticleById(article);
    }
    @RabbitListener(queues = RabbitMqState.delete)
    private void delete(Long[] ids){
        articleService.deleteArticle(ids);
    }
}
