package cn.haoran.modules.palaeobios.mapper;


import cn.haoran.modules.palaeobios.entity.Picture;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
@Mapper
public interface PictureMapper extends BaseMapper<Picture> {

}
