package cn.haoran.modules.palaeobios.controller;


import cn.haoran.common.exception.AppException;
import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.ResultInfo;
import cn.haoran.modules.palaeobios.entity.Article;
import cn.haoran.modules.palaeobios.service.ArticleService;
import cn.haoran.modules.palaeobios.state.RabbitMqState;
import cn.haoran.modules.sys.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  文章前端控制器
 *
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
@RestController
@RequestMapping("/article")
@Api("文章接口列表")
public class ArticleController extends AbstractController {
    private final
    RabbitTemplate rabbitTemplate;
    private final
    ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService, RabbitTemplate rabbitTemplate) {
        this.articleService = articleService;
        this.rabbitTemplate = rabbitTemplate;
    }

    @GetMapping("/list")
    @ApiOperation("分页列表")
    public ResultInfo list(@RequestParam Map<String, Object> params){
        PageUtils page = articleService.queryPage(params);
        return ResultInfo.ok().put("page",page);
    }
    @GetMapping("/search")
    @ApiOperation("分页列表")
    public ResultInfo search(@RequestParam Map<String, Object> params)  {
        List<Article> list = null;
        try {
            list = articleService.searchArticleByContent(params);
        } catch (IOException e) {
            throw new AppException("全文检索遇到未知错误");
        }
        return ResultInfo.ok().put("page",list);
    }
    @PostMapping("save")
    @ApiOperation("保存")
    public ResultInfo save(@RequestBody @Valid Article article){
        article.setCreateDate(LocalDateTime.now());
        article.setCreateId(getUserId());
        article.setCreateName(getUser().getUsername());
        articleService.insert(article);
        rabbitTemplate.convertAndSend(RabbitMqState.insert,article);
        return ResultInfo.ok().put("article",article);
    }

    @GetMapping("/info/{id}")
    @ApiOperation("根据id查询")
    public ResultInfo info(@PathVariable("id") Long id){
        Article article = articleService.getArticleById(id);
        return ResultInfo.ok().put("article",article);
    }
    @PostMapping("/delete")
    @ApiOperation("批量删除或删除")
    public ResultInfo delete(@RequestBody Long[] ids){
        rabbitTemplate.convertAndSend(RabbitMqState.delete,ids);
        return ResultInfo.ok();
    }
    @PostMapping("/update")
    @ApiOperation("修改信息")
    public ResultInfo update(@RequestBody @Valid Article article){
        rabbitTemplate.convertAndSend(RabbitMqState.update,article);
        return ResultInfo.ok();
    }

}
