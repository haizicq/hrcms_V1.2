package cn.haoran.modules.palaeobios.controller;

import cn.haoran.common.utils.ResultInfo;
import cn.haoran.modules.palaeobios.entity.LogSheetEntity;
import cn.haoran.modules.palaeobios.service.LogSheetService;
import cn.hutool.core.date.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/log/sheet")
@Api("文章浏览记录接口列表")
public class LogSheetController {
    @Autowired
    LogSheetService logSheetService;
    @GetMapping("list")
    @ApiOperation("获得日志列表")
    public ResultInfo list(String date){
        List list = null;
        if (StringUtils.isNotBlank(date)){
            list = logSheetService.findByDate(date);
        }else{
            list = logSheetService.findAll();
        }
        return ResultInfo.ok().put("list",list);
    }
    @GetMapping("count")
    @ApiOperation("统计数据")
    public ResultInfo countData(){
        List<LogSheetEntity> list = logSheetService.countMun();
        return ResultInfo.ok().put("list",list);
    }
}
