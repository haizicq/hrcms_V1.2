package cn.haoran.modules.palaeobios.controller;

import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.ResultInfo;
import cn.haoran.modules.palaeobios.entity.Article;
import cn.haoran.modules.palaeobios.entity.Link;
import cn.haoran.modules.palaeobios.entity.Picture;
import cn.haoran.modules.palaeobios.service.LinkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/link")
@Api("快速链接接口列表")
public class LinkController {
    @Autowired
    LinkService linkService;

    @GetMapping("/list")
    @ApiOperation("分页列表")
    public ResultInfo list(@RequestParam Map<String, Object> params) {
        PageUtils page = linkService.queryPage(params);
        return ResultInfo.ok().put("page", page);
    }

    @PostMapping("/save")
    @ApiOperation("保存")
    public ResultInfo save(@RequestBody Link link){
        linkService.insert(link);
        return ResultInfo.ok().put("link",link);
    }

    @PostMapping("/upload")
    @ApiOperation("上传")
    @Transactional
    /* 图片接口 */
    public ResultInfo upload(@RequestParam("upFile") MultipartFile file, Link link) {
        /* String path = fastDFSUtils.uploadFile(file);*/
        if (file.isEmpty()) {
            return ResultInfo.ok();
        }
        // 确定保存到哪个文件夹
        String path = ClassUtils.getDefaultClassLoader().getResource("static").getPath();
        String parentPath = path + "/hrcms/images/LinkImg";
        File parent = new File(parentPath);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        // 确定保存的文件名
        String originalFilename = file.getOriginalFilename();
        String suffix = "";
        int beginIndex = originalFilename.lastIndexOf(".");
        if (beginIndex != -1) {
            suffix = originalFilename.substring(beginIndex);
        }
        String child = UUID.randomUUID().toString() + suffix;

        // 确定保存到哪个文件
        File dest = new File(parent, child);
        // 保存头像文件
        try {
            file.transferTo(dest);
        } catch (IllegalStateException | IOException e) {
            /*.println("文件上传错误:" + e.getMessage());*/
            return ResultInfo.error("文件上传未知错误");
//					throw new FileStateException(
//						"上传头像失败！所选择的文件已经不可用！");
        } // 抛出异常：FileIOException
        //					throw new FileIOException(
        //						"上传头像失败！读写数据时出现错误！");
        link.setImgUrl(child);
        linkService.insertOrUpdate(link);
        return ResultInfo.ok();
    }

    @GetMapping("/info/{id}")
    @ApiOperation("根据id查询")
    public ResultInfo info(@PathVariable("id") Long id) {
        Link picture = linkService.selectById(id);
        return ResultInfo.ok().put("link", picture);
    }

    @PostMapping("/delete")
    @ApiOperation("批量删除或删除")
    public ResultInfo delete(@RequestBody Long[] ids) {
        List<Long> list = Arrays.asList(ids);
        linkService.deleteBatchIds(list);
        return ResultInfo.ok();
    }

    @PostMapping("/update")
    @ApiOperation("修改信息")
    public ResultInfo update(@RequestBody @Valid Link picture) {
        linkService.updateById(picture);
        return ResultInfo.ok();
    }
}
