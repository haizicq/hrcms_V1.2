package cn.haoran.modules.palaeobios.config;

import cn.haoran.modules.palaeobios.state.RabbitMqState;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * cms消息队列配置
 */
@Configuration
public class RabbitConfig {
    // 创建文章写入队列
    @Bean
    public Queue getInsertQueue() {
        return new Queue(RabbitMqState.insert);
    }
    // 创建文章修改队列
    @Bean
    public Queue getUpdateQueue() {
        return new Queue(RabbitMqState.update);
    }
    // 创建文章删除队列
    @Bean
    public Queue getDeleteQueue() {
        return new Queue(RabbitMqState.delete);
    }
}
