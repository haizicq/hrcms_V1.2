package cn.haoran.modules.palaeobios.service.impl;

import cn.haoran.modules.palaeobios.entity.LogEntity;
import cn.haoran.modules.palaeobios.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    MongoTemplate mongoTemplate;
    @Override
    public List<LogEntity> findAll() {
        List<LogEntity> list = mongoTemplate.findAll(LogEntity.class);
        return list;
    }

    @Override
    public List<LogEntity> findAllByDate(String dateTime) {
        Query query = new Query(Criteria.where("createDate").regex(dateTime));
        List<LogEntity> list = mongoTemplate.find(query,LogEntity.class);
        return list;
    }
}
