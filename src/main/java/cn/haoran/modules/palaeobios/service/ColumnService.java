package cn.haoran.modules.palaeobios.service;


import cn.haoran.common.utils.PageUtils;
import cn.haoran.modules.palaeobios.entity.Column;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
public interface ColumnService extends IService<Column> {
    /**
     * 分页
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);
}
