package cn.haoran.modules.palaeobios.service;

import cn.haoran.modules.palaeobios.entity.LogSheetEntity;

import java.util.List;
import java.util.Map;

public interface LogSheetService {
    /**
     * 查询多有浏览记录
     * @return
     */
    List<LogSheetEntity> findAll();

    /**
     * 按数据查询浏览记录
     */
    List<LogSheetEntity> findByDate(String date);
    /**
     * 统计时间
     */
    List<LogSheetEntity> countMun();
}
