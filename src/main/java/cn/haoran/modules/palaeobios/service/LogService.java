package cn.haoran.modules.palaeobios.service;

import cn.haoran.modules.palaeobios.entity.LogEntity;

import java.time.LocalDateTime;
import java.util.List;

public interface LogService {
    /**
     * 查询日志所以数据
     * @return 结果集
     */
    List<LogEntity> findAll();
    /**
     * 按日期查询时间
     */
    List<LogEntity> findAllByDate(String dateTime);
}
