package cn.haoran.modules.palaeobios.service.impl;


import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.Query;
import cn.haoran.modules.palaeobios.entity.Article;
import cn.haoran.modules.palaeobios.entity.Column;
import cn.haoran.modules.palaeobios.mapper.ColumnMapper;
import cn.haoran.modules.palaeobios.service.ColumnService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author haoran
 * @since 2019-12-12
 */
@Service
public class ColumnServiceImpl extends ServiceImpl<ColumnMapper, Column> implements ColumnService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("name");
        Long parentId = Long.parseLong(params.get("parentId").toString());
        Page<Column> page = this.selectPage(
                new Query<Column>(params).getPage(),
                new EntityWrapper<Column>().like(StringUtils.isNotBlank(name),"name",name).eq("parent_id",parentId));
        return new PageUtils(page);
    }
}
