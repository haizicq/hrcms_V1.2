package cn.haoran.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("sys_upload_file")
public class UploadFileEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Long id;
    /**
     * 上传文件名称
     */
    private String name;
    /**
     * 上传时间
     */
    private Date createDate;
    /**
     * 保存路径
     */
    private String url;

    /**
     * 文件后缀名
     */
    private String suffix;
    /**
     * 上传用户id
     */
    private Long userId;
    /**
     * 上传用户名
     */
    private String userName;
    /**
     * 缩略图url
     */
    private String thumbUrl;
    /**
     * 上级文件夹id
     */
    private Long parentFolderId;
    /**
     * 文件夹全路径
     */
    private String parentFolderPath;

    /**
     * 静态资源服务器ip及端口
     */
    @TableField(exist = false)
    private String serverIp;
    /**
     * 文件组名
     */
    @TableField(exist = false)
    private String group;

}
