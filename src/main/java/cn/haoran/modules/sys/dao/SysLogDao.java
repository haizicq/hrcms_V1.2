package cn.haoran.modules.sys.dao;

import cn.haoran.modules.sys.entity.SysLogEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 *
 * Create by hanran on 2019/6/15
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {

}
