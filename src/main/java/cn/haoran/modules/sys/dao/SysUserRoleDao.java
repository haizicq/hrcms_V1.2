package cn.haoran.modules.sys.dao;

import cn.haoran.modules.sys.entity.SysUserRoleEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 * Create by hanran on 2019/6/15
 */
@Mapper
public interface SysUserRoleDao extends BaseMapper<SysUserRoleEntity> {

    /**
     * 根据用户ID，获取角色ID列表
     */
    List<Long> queryRoleIdList(Long userId);


    /**
     * 根据角色ID数组，批量删除
     */
    int deleteBatch(Long[] roleIds);

    /**
     * 根据用户ID数组批量删除
     * @param userIds
     * @return
     */
    int deleteBatchByUserIds(Long[] userIds);
}
