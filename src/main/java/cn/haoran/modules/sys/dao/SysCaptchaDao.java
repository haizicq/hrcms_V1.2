package cn.haoran.modules.sys.dao;

import cn.haoran.modules.sys.entity.SysCaptchaEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 验证码
 *
 * Create by hanran on 2019/6/15
 */
@Mapper
public interface SysCaptchaDao extends BaseMapper<SysCaptchaEntity> {

}
