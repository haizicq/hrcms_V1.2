package cn.haoran.modules.sys.service;

import cn.haoran.modules.sys.entity.SysCaptchaEntity;
import com.baomidou.mybatisplus.service.IService;

import java.awt.image.BufferedImage;

/**
 * 验证码
 *
 * Create by hanran on 2019/6/15
 */
public interface SysCaptchaService extends IService<SysCaptchaEntity> {

    /**
     * 获取图片验证码
     */
    BufferedImage getCaptcha(String uuid);

    /**
     * 验证码效验
     * @param uuid  uuid
     * @param code  验证码
     * @return  true：成功  false：失败
     */
    boolean validate(String uuid, String code);
}