package cn.haoran.modules.sys.service.impl;

import cn.haoran.common.exception.AppException;
import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.Query;
import cn.haoran.modules.sys.dao.SysConfigDao;
import cn.haoran.modules.sys.entity.SysConfigEntity;
import cn.haoran.modules.sys.redis.SysConfigRedis;
import cn.haoran.modules.sys.service.SysConfigService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Map;

/**
 * Create by hanran on 2019/6/15
 */
@Service("sysConfigService")
public class SysConfigServiceImpl extends ServiceImpl<SysConfigDao, SysConfigEntity> implements SysConfigService {
    @Autowired
    private SysConfigRedis sysConfigRedis;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String paramKey = (String)params.get("paramKey");

        Page<SysConfigEntity> page = this.selectPage(
                new Query<SysConfigEntity>(params).getPage(),
                new EntityWrapper<SysConfigEntity>()
                        .like(StringUtils.isNotBlank(paramKey),"param_key", paramKey)
                        .eq("status", 1)
        );

        return new PageUtils(page);
    }

    @Override
    public void save(SysConfigEntity config) {
        this.insert(config);
        /*sysConfigRedis.saveOrUpdate(config);*/
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SysConfigEntity config) {
        this.updateAllColumnById(config);
        /*sysConfigRedis.saveOrUpdate(config);*/
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateValueByKey(String key, String value) {
        baseMapper.updateValueByKey(key, value);
        /*sysConfigRedis.delete(key);*/
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatch(Long[] ids) {
        for(Long id : ids){
            SysConfigEntity config = this.selectById(id);
            /*sysConfigRedis.delete(config.getParamKey());*/
        }

        this.deleteBatchIds(Arrays.asList(ids));
    }

    @Override
    public String getValue(String key) {
        SysConfigEntity config = null;/*sysConfigRedis.get(key);*/
        if(config == null){
            config = baseMapper.queryByKey(key);
            /*sysConfigRedis.saveOrUpdate(config);*/
        }

        return config == null ? null : config.getParamValue();
    }

    @Override
    public <T> T getConfigObject(String key, Class<T> clazz) {
        String value = getValue(key);
        if(StringUtils.isNotBlank(value)){
            return new Gson().fromJson(value, clazz);
        }

        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new AppException("获取参数失败");
        }
    }
}
