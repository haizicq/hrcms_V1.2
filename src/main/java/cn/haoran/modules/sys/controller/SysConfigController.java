package cn.haoran.modules.sys.controller;

import cn.haoran.common.utils.PageUtils;
import cn.haoran.common.utils.ResultInfo;
import cn.haoran.common.validator.ValidatorUtils;
import cn.haoran.modules.palaeobios.entity.Article;
import cn.haoran.modules.sys.entity.SysConfigEntity;
import cn.haoran.modules.sys.service.SysConfigService;
import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.invoke.util.Wrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统配置信息
 *
 * Create by hanran on 2019/6/15
 */
@RestController
@RequestMapping("/sys/config")
public class SysConfigController extends AbstractController {
    @Autowired
    private SysConfigService sysConfigService;

    @GetMapping("/getAdminHost")
    public ResultInfo getAdminHost(){
        /* 获得当前项目的健康监控的url */
         String config = sysConfigService.getValue("springBootAdmin");
         return ResultInfo.ok().put("config",config);
    }

    @GetMapping("/getVerifySize")
    public ResultInfo verifySize(Integer thisSize){
        /* 获得当前项目的健康监控的url */
        String config = sysConfigService.getValue("cms_size");
        if(config == null){
            return ResultInfo.ok().put("res",true);
        }
        Gson gson = new Gson();
        Map map = new HashMap<String, Object>();
        map = gson.fromJson(config, map.getClass());
        /* 大小 */
        Integer size = Integer.parseInt(map.get("size").toString());
        if (size < thisSize){
            return ResultInfo.ok().put("res",false);
        }
        return ResultInfo.ok().put("res",true);
    }
    /**
     * 所有配置列表
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:config:list")
    public ResultInfo list(@RequestParam Map<String, Object> params){
        PageUtils page = sysConfigService.queryPage(params);

        return ResultInfo.ok().put("page", page);
    }

    /**
     * 查找相应的配置项配置列表
     */
    @GetMapping("/like")
    @RequiresPermissions("sys:config:list")
    public ResultInfo likeList(@RequestParam String configName){
        List<SysConfigEntity> list = sysConfigService.selectList(
                new EntityWrapper<SysConfigEntity>().like(StringUtils.isNotBlank(configName),"param_key",configName)
        );
        return ResultInfo.ok().put("list",list);
    }

    /**
     * 配置信息
     */
    @GetMapping("/info/{id}")
    @RequiresPermissions("sys:config:info")
    public ResultInfo info(@PathVariable("id") Long id){
        SysConfigEntity config = sysConfigService.selectById(id);

        return ResultInfo.ok().put("config", config);
    }

    /**
     * 保存配置
     */
    //@SysLog("保存配置")
    @PostMapping("/save")
    @RequiresPermissions("sys:config:save")
    public ResultInfo save(@RequestBody SysConfigEntity config){
        ValidatorUtils.validateEntity(config);

        sysConfigService.save(config);

        return ResultInfo.ok();
    }

    /**
     * 修改配置
     */
    //@SysLog("修改配置")
    @PostMapping("/update")
    @RequiresPermissions("sys:config:update")
    public ResultInfo update(@RequestBody SysConfigEntity config){
        ValidatorUtils.validateEntity(config);

        sysConfigService.update(config);

        return ResultInfo.ok();
    }

    /**
     * 删除配置
     */
    //@SysLog("删除配置")
    @PostMapping("/delete")
    @RequiresPermissions("sys:config:delete")
    public ResultInfo delete(@RequestBody Long[] ids){
        sysConfigService.deleteBatch(ids);
        return ResultInfo.ok();
    }


}
