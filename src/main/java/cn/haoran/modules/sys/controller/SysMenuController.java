package cn.haoran.modules.sys.controller;

import cn.haoran.common.exception.AppException;
import cn.haoran.common.utils.Constant;
import cn.haoran.common.utils.ResultInfo;
import cn.haoran.modules.sys.entity.SysMenuEntity;
import cn.haoran.modules.sys.service.ShiroService;
import cn.haoran.modules.sys.service.SysMenuService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * Create by hanran on 2019/6/15
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController {
    @Autowired
    private SysMenuService sysMenuService;
    @Autowired
    private ShiroService shiroService;

    /**
     * 导航菜单
     */
    @GetMapping("/nav")
    public ResultInfo nav(){
        List<SysMenuEntity> menuList = sysMenuService.getUserMenuList(getUserId());
        Set<String> permissions = shiroService.getUserPermissions(getUserId());
        return ResultInfo.ok().put("menuList", menuList).put("permissions", permissions);
    }

    /**
     * 所有菜单列表
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:menu:list")
    public List<SysMenuEntity> list(){
        List<SysMenuEntity> menuList = sysMenuService.selectList(null);
        for(SysMenuEntity sysMenuEntity : menuList){
            SysMenuEntity parentMenuEntity = sysMenuService.selectById(sysMenuEntity.getParentId());
            if(parentMenuEntity != null){
                sysMenuEntity.setParentName(parentMenuEntity.getName());
            }
        }

        return menuList;
    }

    /**
     * 选择菜单(添加、修改菜单)
     */
    @GetMapping("/select")
    @RequiresPermissions("sys:menu:select")
    public ResultInfo select(){
        //查询列表数据
        List<SysMenuEntity> menuList = sysMenuService.queryNotButtonList();

        //添加顶级菜单
        SysMenuEntity root = new SysMenuEntity();
        root.setMenuId(0L);
        root.setName("一级菜单");
        root.setParentId(-1L);
        root.setOpen(true);
        menuList.add(root);

        return ResultInfo.ok().put("menuList", menuList);
    }

    /**
     * 菜单信息
     */
    @GetMapping("/info/{menuId}")
    @RequiresPermissions("sys:menu:info")
    public ResultInfo info(@PathVariable("menuId") Long menuId){
        SysMenuEntity menu = sysMenuService.selectById(menuId);
        return ResultInfo.ok().put("menu", menu);
    }

    @GetMapping("/resource/{menuId}")
    @RequiresPermissions("sys:menu:info")
    public ResultInfo listResources(@PathVariable("menuId") Long menuId) {
        List<SysMenuEntity> resourceList = sysMenuService.listResourcesByParentId(menuId);
        return ResultInfo.ok().put("resourceList", resourceList);
    }

    /**
     * 保存
     */
    //@SysLog("保存菜单")
    @PostMapping("/save")
    @RequiresPermissions("sys:menu:save")
    public ResultInfo save(@RequestBody SysMenuEntity menu){
        //数据校验
        verifyForm(menu);

        sysMenuService.insert(menu);

        return ResultInfo.ok();
    }

    /**
     * 修改
     */
    //@SysLog("修改菜单")
    @PostMapping("/update")
    @RequiresPermissions("sys:menu:update")
    public ResultInfo update(@RequestBody SysMenuEntity menu){
        //数据校验
        verifyForm(menu);

        sysMenuService.updateById(menu);

        return ResultInfo.ok();
    }

    /**
     * 删除
     */
    //@SysLog("删除菜单")
    @PostMapping("/delete/{menuId}")
    @RequiresPermissions("sys:menu:delete")
    public ResultInfo delete(@PathVariable("menuId") long menuId){
        if(menuId <= 31){
            return ResultInfo.error("系统菜单，不能删除");
        }

        //判断是否有子菜单或按钮
        List<SysMenuEntity> menuList = sysMenuService.queryListParentId(menuId);
        if(menuList.size() > 0){
            return ResultInfo.error("请先删除子菜单或按钮");
        }

        sysMenuService.delete(menuId);

        return ResultInfo.ok();
    }

    /**
     * 验证参数是否正确
     */
    private void verifyForm(SysMenuEntity menu){
        if(StringUtils.isBlank(menu.getName())){
            throw new AppException("菜单名称不能为空");
        }

        if(menu.getParentId() == null){
            throw new AppException("上级菜单不能为空");
        }

        //菜单
        if(menu.getType() == Constant.MenuType.MENU.getValue()){
            if(StringUtils.isBlank(menu.getUrl())){
                throw new AppException("菜单URL不能为空");
            }
        }

        //上级菜单类型
        int parentType = Constant.MenuType.CATALOG.getValue();
        if(menu.getParentId() != 0){
            SysMenuEntity parentMenu = sysMenuService.selectById(menu.getParentId());
            parentType = parentMenu.getType();
        }

        //目录、菜单
        if(menu.getType() == Constant.MenuType.CATALOG.getValue() ||
                menu.getType() == Constant.MenuType.MENU.getValue()){
            if(parentType != Constant.MenuType.CATALOG.getValue()){
                throw new AppException("上级菜单只能为目录类型");
            }
            return ;
        }

        //按钮
        if(menu.getType() == Constant.MenuType.BUTTON.getValue()){
            if(parentType != Constant.MenuType.MENU.getValue()){
                throw new AppException("上级菜单只能为菜单类型");
            }
            return ;
        }
    }
}
