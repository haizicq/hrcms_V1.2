package cn.haoran.modules.sys.config;


import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
@Component
@RabbitListener(queues = "sb.hello")
public class HelloReceiver {
    @Autowired
    RabbitTemplate rabbitTemplate;
    public void send(){
        // 消息持久化
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
        rabbitTemplate.convertAndSend(IExchangeState.QUEUE_USER,messageProperties);
    }


    @RabbitHandler
    public void process(String hello) {
        .println("接收到邮件消息 : " + hello);
    }
}
*/
