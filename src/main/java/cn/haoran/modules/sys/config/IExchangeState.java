package cn.haoran.modules.sys.config;

/**
 * 定义rabbitMq  的 交换机(名称) 队列名(名称)
 * 交换机  命名交换类型 1:常规交换机 2.主题 3 广播
 * 队列   队列变量名命名 :必须前面必须加 QUEUE_XXX 来表示队列名称
 */
public interface IExchangeState {
    String QUEUE_HELLO = "sb.hello";
    String QUEUE_USER = "sb.user";
    //常用队列名
    String QUEUE_TOPIC_EMAIL = "sb.info.email";
    String QUEUE_TOPIC_USER = "sb.info.user";
    String RK_EMAIL = "sb.info.email";
    String RK_USER = "sb.info.user";

    String EXCHANGE_TOPIC = "sb.exchange";
    String EXCHANGE_FANOUT = "sb.fanout.exchange";
}
