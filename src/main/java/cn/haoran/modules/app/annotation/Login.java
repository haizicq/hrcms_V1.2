package cn.haoran.modules.app.annotation;

import java.lang.annotation.*;

/**
 * app登录效验
 *
 * Create by hanran on 2019/6/15
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Login {
}
