package cn.haoran.modules.app.dao;

import cn.haoran.modules.app.entity.UserEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by hanran on 2019/6/15
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {

}
