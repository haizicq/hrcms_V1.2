package cn.haoran.modules.app.controller;

import cn.haoran.common.utils.ResultInfo;

import cn.haoran.modules.app.annotation.Login;
import cn.haoran.modules.app.annotation.LoginUser;
import cn.haoran.modules.app.entity.UserEntity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.*;

/**
 * Create by hanran on 2019/6/16
 */
@RestController
@RequestMapping("/app")
@Api("APP测试接口")
public class AppTestController {
    @Login
    @PostMapping("userInfo")
    @ApiOperation("获取用户信息")
    public ResultInfo userInfo(@LoginUser UserEntity user){
        return ResultInfo.ok().put("user", user);
    }

    @Login
    @GetMapping("userId")
    @ApiOperation("获取用户ID")
    public ResultInfo userInfo(@RequestAttribute("userId") Integer userId){
        return ResultInfo.ok().put("userId", userId);
    }

}
