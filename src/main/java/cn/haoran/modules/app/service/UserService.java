package cn.haoran.modules.app.service;

import cn.haoran.modules.app.entity.UserEntity;
import cn.haoran.modules.app.form.LoginForm;
import com.baomidou.mybatisplus.service.IService;

/**
 * Create by hanran on 2019/6/15
 */
public interface UserService extends IService<UserEntity> {

    UserEntity queryByMobile(String mobile);

    /**
     * 用户登录
     * @param form    登录表单
     * @return        返回用户ID
     */
    long login(LoginForm form);
}
