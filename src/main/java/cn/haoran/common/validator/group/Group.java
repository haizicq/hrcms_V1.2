package cn.haoran.common.validator.group;

import javax.validation.GroupSequence;

/**
 * 定义校验顺序，如果AddGroup组失败，则UpdateGroup组不会再校验
 *
 * Create by hanran on 2019/6/15
 */
@GroupSequence({AddGroup.class, UpdateGroup.class})
public interface Group {
}
