package cn.haoran.common.utils;

/**
 * Redis所有Key
 * Create by hanran on 2019/6/15
 */
public class RedisKeys {
    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }
}
