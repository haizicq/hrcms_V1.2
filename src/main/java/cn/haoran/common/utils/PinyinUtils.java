package cn.haoran.common.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import org.apache.commons.lang.StringUtils;

/**
 * Create by hanran on 2019/6/22
 */
public class PinyinUtils {
    public static String toPinyin(String name) throws Exception {
        if(StringUtils.isBlank(name)) {
            return "";
        }
        char[] array = name.toCharArray();
        StringBuilder pinyin = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (Character.toString(array[i]).matches("[\\u4E00-\\u9FA5]+")) {
                pinyin.append(PinyinHelper.toHanyuPinyinStringArray(array[i])[0].charAt(0));
            } else {
                pinyin.append(array[i]);
            }
        }
        return pinyin.toString().toUpperCase();
    }
}
