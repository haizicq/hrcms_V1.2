package cn.haoran.common.utils;

import java.util.HashMap;

/**
 * Map工具类
 * Create by hanran on 2019/6/15
 */
public class MapUtils extends HashMap<String, Object> {

    @Override
    public MapUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}