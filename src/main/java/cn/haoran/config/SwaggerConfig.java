package cn.haoran.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置
 * Create by hanran on 2019/6/15
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/hrcms/index.html")
                //.addResourceLocations("classpath:/META-INF/resources/visual/")
                //.addResourceLocations("classpath:/resources/")
                .addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/hrcms/**")
                .addResourceLocations("classpath:/static/hrcms/");
       /* registry.addResourceHandler("visual/js/**")
                .addResourceLocations("classpath:/static/visual/js/");*/

        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");

    }

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))           //加了ApiOperation注解的方法，生成接口文档
                //.apis(RequestHandlerSelectors.basePackage("cn.haoran.modules.job.controller"))  //包下的类，生成接口文档
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("浩然基础架构")
                .description("接口文档")
                .termsOfServiceUrl("")
                .version("0.0.1")
                .build();
    }

}