package cn.haoran.config;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;
import org.springframework.jmx.support.RegistrationPolicy;

/**
 * FastDFS 配置类
 *
 * Create by qinji on 2019/10/9
 */
@Configuration
@Import(FdfsClientConfig.class)
/**
 *  解决jmx重复注册bean的问题!
 */
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
public class FastDFSConfig {

}
